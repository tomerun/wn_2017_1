import java.io.*;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;
import java.util.SplittableRandom;

public class Main {

  private static final int SHIFT = 1 << 15;
  private static final int BLOCK = 25;
  static String encodingFileName;
  static String directoryName;

  private static class RangeCoder {

    private static final long MAX_RANGE = 0x1_0000_0000L;
    private static final long MIN_RANGE = MAX_RANGE >> 8;
    private static final int DECODE_HINT_BITS = 8;
    int histoBase, histoLen;
    int[] histogram, histoSum;
    double[] bitCounts;
    long low, range, histoTotal;
    int buf, ffNum;
    boolean encodeInitial;
    int[] decodeHint = new int[(1 << DECODE_HINT_BITS) + 1];

    void build(int[] histogram) {
      this.histogram = histogram.clone();
      this.histoSum = new int[histogram.length + 1];
      this.bitCounts = new double[histogram.length];
      this.histoBase = 1 << 30;
      int max = 0;
      for (int i = 0; i < histogram.length; i++) {
        if (histogram[i] != 0) {
          this.histoBase = Math.min(this.histoBase, i);
          this.histoLen = Math.max(this.histoLen, i - this.histoBase + 1);
          max = Math.max(max, this.histogram[i]);
        }
      }
      int bits = Math.max(0, 16 - Integer.numberOfLeadingZeros(max));
      for (int i = this.histoBase; i < this.histoBase + this.histoLen; i++) {
        this.histogram[i] >>= bits;
        if (this.histogram[i] == 0) this.histogram[i] = 1;
        this.histoSum[i + 1] = this.histoSum[i] + this.histogram[i];
      }
      this.low = 0;
      this.range = MAX_RANGE;
      this.histoTotal = this.histoSum[this.histoBase + this.histoLen];
      double maxBits = 9999;
      for (int i = 0; i < histogram.length; i++) {
        if (histogram[i] != 0) {
          bitCounts[i] = -Math.log(1.0 * this.histogram[i] / this.histoTotal) / Math.log(2);
          maxBits = Math.max(maxBits, bitCounts[i]);
        }
      }
      for (int i = 0; i < histogram.length; i++) {
        if (bitCounts[i] == 0) bitCounts[i] = maxBits;
      }
    }

    void startEncode(OutputStream out) {
      write16(out, this.histoBase);
      write16(out, this.histoLen);
      for (int i = 0; i < this.histoLen; i++) {
        write16(out, this.histogram[i + this.histoBase]);
      }
      this.encodeInitial = true;
    }

    void encode(int v, OutputStream out) throws Exception {
      this.low += this.range * this.histoSum[v] / this.histoTotal;
      this.range = this.range * this.histogram[v] / this.histoTotal;
//      if (this.range == 0) {
//        System.err.printf("encode:%d %x %x\n", v, this.histogram[v], this.histoTotal);
//        System.err.printf("low:%08x range:%08x\n", this.low, this.range);
//        throw new RuntimeException("range zero");
//      }
      while (this.range < MIN_RANGE) {
        this.range <<= 8;
        this.low <<= 8;
        int ex = (int) (this.low / MAX_RANGE);
        this.low &= (MAX_RANGE - 1);
        if (ex < 0xFF) {
          if (this.encodeInitial) {
            this.encodeInitial = false;
          } else {
            out.write(this.buf);
          }
          for (int i = 0; i < this.ffNum; i++) {
            out.write(0xFF);
          }
          this.buf = ex;
          this.ffNum = 0;
        } else if (0x100 <= ex && ex < 0x1FF) {
          out.write(this.buf + 1);
          for (int i = 0; i < this.ffNum; i++) {
            out.write(0);
          }
          buf = ex & 0xFF;
          this.ffNum = 0;
        } else if (ex == 0x1FF) {
          if (this.ffNum > 0) {
            out.write(this.buf + 1);
            for (int i = 0; i < this.ffNum - 1; i++) {
              out.write(0);
            }
            buf = 0;
          } else {
            buf++;
          }
          this.ffNum = 1;
        } else {
          this.ffNum++;
        }
      }
    }

    void flush(OutputStream out) throws Exception {
      if (this.low >= MAX_RANGE) {
        out.write(this.buf + 1);
        for (int i = 0; i < this.ffNum; i++) {
          out.write(0);
        }
      } else {
        out.write(this.buf);
        for (int i = 0; i < this.ffNum; i++) {
          out.write(0xFF);
        }
      }
      out.write((int) (this.low >> 24));
      out.write((int) (this.low >> 16));
      out.write((int) (this.low >> 8));
      out.write((int) (this.low));
    }

    static RangeCoder read(InputStream in) {
      RangeCoder rc = new RangeCoder();
      rc.histoBase = read16(in);
      rc.histoLen = read16(in);
      rc.histogram = new int[rc.histoLen];
      rc.histoSum = new int[rc.histoLen + 1];
      for (int i = 0; i < rc.histoLen; i++) {
        rc.histogram[i] = read16(in);
        rc.histoSum[i + 1] = rc.histoSum[i] + rc.histogram[i];
      }
      rc.histoTotal = rc.histoSum[rc.histogram.length];
      int prevPos = 0;
      for (int i = 0; i < rc.histoLen; i++) {
        int pos = (int) ((rc.histoSum[i + 1] * (1L << DECODE_HINT_BITS) + rc.histoTotal - 1) / rc.histoTotal);
        for (int j = prevPos; j < pos; j++) {
          rc.decodeHint[j] = i;
        }
        prevPos = pos;
      }
      rc.decodeHint[rc.decodeHint.length - 1] = rc.histoLen - 1;
//      System.err.println("hint:" + Arrays.toString(rc.decodeHint));
//      System.err.println("histoSum:" + Arrays.toString(rc.histoSum));
//      System.err.println("histoTotal"+ rc.histoTotal);
//      System.err.printf("base:%x len:%x total:%x\n", rc.histoBase, rc.histoLen, rc.histoTotal);
      return rc;
    }

    void decode(InputStream in, int[] out, int len) throws Exception {
      for (int i = 0; i < 4; i++) {
        this.low <<= 8;
        this.low |= in.read();
      }
      this.range = MAX_RANGE;
      for (int i = 0; i < len; i++) {
//        long tmp = this.low << 8;
//        int pos = (int)(tmp / this.range) << 8;
//        tmp = (tmp % this.range) << 8;
//        pos += tmp / this.range;
        int pos = (int) (this.low * (1L << DECODE_HINT_BITS) / this.range);
        int lo = this.decodeHint[pos];
        int hi = this.decodeHint[pos + 1] + 1;
        while (hi - lo > 1) {
          int mid = (lo + hi) / 2;
          long v1 = this.range * this.histoSum[mid] / this.histoTotal;
          if (v1 > this.low) {
            hi = mid;
          } else if (v1 == this.low) {
            lo = mid;
            break;
          } else {
            long v2 = this.range * this.histoSum[mid + 1] / this.histoTotal;
            if (v2 < this.low) {
              lo = mid + 1;
            } else if (v2 == this.low) {
              lo = mid + 1;
              break;
            } else {
              lo = mid;
              break;
            }
          }
        }
        int symbol = lo;
        out[i] = symbol + this.histoBase;
        this.low -= this.range * this.histoSum[symbol] / this.histoTotal;
        this.range = this.range * this.histogram[symbol] / this.histoTotal;
//        if (this.range == 0) {
//          System.err.printf("decode:%d %x %x\n", symbol, this.histogram[symbol], this.histoTotal);
//          System.err.printf("i:%d v:%d, range:%x, low:%x\n", i, out[i] - SHIFT, range, low);
//          throw new RuntimeException("range zero");
//        }
        while (this.range < MIN_RANGE) {
          this.range <<= 8;
          this.low <<= 8;
          this.low |= in.read();
          this.low &= 0xFFFFFFFFL;
        }
      }
    }
  }

  private static class Encoder {

    OutputStream out;
    int[][] encValue = new int[6000][10000];
    SplittableRandom rnd = new SplittableRandom(42);

    void encode(ImageFile[] files) throws Exception {
      try {
        out = new BufferedOutputStream(new FileOutputStream(encodingFileName));
        encodeHeader(files);
        for (int i = 0; i < files.length; i += 4) {
          files[i].read();
          encodeFirstFile(files[i]);
          for (int j = 1; j < 4; j++) {
            files[i + j].read();
            encodeFollowingFile(files[i + j], files[i + j - 1]);
          }
          for (int j = 0; j < 4; j++) {
            files[j].discard();
          }
        }
      } finally {
        out.close();
        out = null;
      }
    }

    void encodeHeader(ImageFile[] files) throws Exception {
      out.write(files.length);
      for (ImageFile file : files) {
        for (char c : file.name.toCharArray()) {
          out.write(c);
        }
        out.write(0);
        out.write(file.size);
        out.write(file.size >>> 8);
        out.write(file.size >>> 16);
        out.write(file.size >>> 24);
      }
    }

    void encodeFirstFile(ImageFile file) throws Exception {
      long startTime = System.currentTimeMillis();
      int[] histogram = new int[1 << 16];
      final int w = file.width();
      final int h = file.height();
      for (int i = 1; i <= h; i++) {
        for (int j = 1; j <= w; j++) {
          encValue[i - 1][j - 1] = (file.values[i][j] - file.values[i][j - 1]) - (file.values[i - 1][j] - file.values[i - 1][j - 1]) + SHIFT;
          histogram[encValue[i - 1][j - 1]]++;
        }
      }
      RangeCoder encoder = new RangeCoder();
      encoder.build(histogram);
      System.err.println("build " + (System.currentTimeMillis() - startTime));
      encoder.startEncode(out);
      for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
          encoder.encode(encValue[i][j], out);
        }
      }
      encoder.flush(out);
      System.err.println("encode " + (System.currentTimeMillis() - startTime));
    }

    void encodeFollowingFile(ImageFile file, ImageFile prevFile) throws Exception {
      long startTime = System.currentTimeMillis();
      int[] histogram = new int[1 << 16];
      final int w = file.width();
      final int h = file.height();
      for (int i = 1; i <= h; i++) {
        for (int j = 1; j <= w; j++) {
          encValue[i - 1][j - 1] = getDiff(file.values, i, j) - getDiff(prevFile.values, i, j) + SHIFT;
          histogram[encValue[i - 1][j - 1]]++;
        }
      }
      RangeCoder encoder = new RangeCoder();
      encoder.build(histogram);
      Arrays.fill(histogram, 0);
      int[][] move = new int[h / BLOCK][w / BLOCK];
      for (int bi = 0; bi < h / BLOCK; bi++) {
        for (int bj = 0; bj < w / BLOCK; bj++) {
          move[bi][bj] = findBestMove(h, w, bi * BLOCK, bj * BLOCK, 0, 0, encoder, file, prevFile);
          int dy = (move[bi][bj] >> 8) - 127;
          int dx = (move[bi][bj] & 0xFF) - 127;
          for (int i = 0; i < BLOCK; i++) {
            int cy = bi * BLOCK + i + 1;
            for (int j = 0; j < BLOCK; j++) {
              int cx = bj * BLOCK + j + 1;
              encValue[cy - 1][cx - 1] = getDiff(file.values, cy, cx) - getDiff(prevFile.values, cy + dy, cx + dx) + SHIFT;
              histogram[encValue[cy - 1][cx - 1]]++;
            }
          }
        }
      }
      encoder = new RangeCoder();
      encoder.build(histogram);
      Arrays.fill(histogram, 0);
      for (int bi = 0; bi < h / BLOCK; bi++) {
        for (int bj = 0; bj < w / BLOCK; bj++) {
          move[bi][bj] = findBestMove(h, w, bi * BLOCK, bj * BLOCK,
              (move[bi][bj] >> 8) - 127, (move[bi][bj] & 0xFF) - 127, encoder, file, prevFile);
          int dy = (move[bi][bj] >> 8) - 127;
          int dx = (move[bi][bj] & 0xFF) - 127;
          for (int i = 0; i < BLOCK; i++) {
            int cy = bi * BLOCK + i + 1;
            for (int j = 0; j < BLOCK; j++) {
              int cx = bj * BLOCK + j + 1;
              encValue[cy - 1][cx - 1] = getDiff(file.values, cy, cx) - getDiff(prevFile.values, cy + dy, cx + dx) + SHIFT;
              histogram[encValue[cy - 1][cx - 1]]++;
            }
          }
        }
      }
      encoder = new RangeCoder();
      encoder.build(histogram);

      System.err.println("build " + (System.currentTimeMillis() - startTime));
      for (int i = 0; i < h / BLOCK; i++) {
        for (int j = 0; j < w / BLOCK; j++) {
          write16(out, move[i][j]);
        }
      }
      encoder.startEncode(out);
      for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
          encoder.encode(encValue[i][j], out);
        }
      }
      encoder.flush(out);
      System.err.println("encode " + (System.currentTimeMillis() - startTime));
    }

    int findBestMove(int h, int w, int y, int x, int idy, int idx, RangeCoder encoder, ImageFile file, ImageFile prevFile) {
      int[][] v = file.values;
      int[][] pv = prevFile.values;
      int ret = ((idy + 127) << 8) | (idx + 127);
      double best = 0;
      for (int i = 0; i < BLOCK; i++) {
        for (int j = 0; j < BLOCK; j++) {
          int diff = getDiff(v, y + i + 1, x + j + 1) - getDiff(pv, y + idy + i + 1, x + idx + j + 1) + SHIFT;
          best += encoder.bitCounts[diff];
        }
      }
//      System.err.println("initial:" + best);
      int my = idy;
      int mx = idx;
      for (int turn = 0; turn < 50; turn++) {
        int dy = my + rnd.nextInt(7) - 3;
        int dx = mx + rnd.nextInt(7) - 3;
        if (dy < -127) dy = -127;
        if (dy > 127) dy = 127;
        if (y + dy < 0) dy = -y;
        if (y + BLOCK + dy > h) dy = h - y - BLOCK;
        if (dx < -127) dx = -127;
        if (dx > 127) dx = 127;
        if (x + dx < 0) dx = -x;
        if (x + BLOCK + dx > w) dx = w - x - BLOCK;
        double sum = 0;
        for (int i = 0; i < BLOCK; i++) {
          int cy = y + i + 1;
          for (int j = 0; j < BLOCK; j++) {
            int cx = x + j + 1;
            int diff = getDiff(v, cy, cx) - getDiff(pv, cy + dy, cx + dx) + SHIFT;
            sum += encoder.bitCounts[diff];
          }
        }
        if (sum < best) {
          best = sum;
          my = dy;
          mx = dx;
          ret = ((my + 127) << 8) | (mx + 127);
        }
      }
//      System.err.println("best:" + best + " dy:" + ((ret >> 8) - 127) + " dx:" + ((ret & 0xFF) - 127));
      return ret;
    }

    static int getDiff(int[][] v, int y, int x) {
      return v[y][x] - v[y][x - 1];
//      return v[y][x] - v[y][x - 1] - (v[y - 1][x] - v[y - 1][x - 1]);
    }
  }

  private static class Decoder {

    InputStream in;

    void decode() throws Exception {
      try {
        in = new BufferedInputStream(new FileInputStream(encodingFileName));
        ImageFile[] files = readHeader();
        for (int i = 0; i < files.length; i += 4) {
          try (OutputStream out = new BufferedOutputStream(
              new FileOutputStream(Paths.get(directoryName, files[i].name).toFile()))) {
            decodeFirstFile(out, files[i]);
          }
          for (int j = 1; j < 4; j++) {
            try (OutputStream out = new BufferedOutputStream(
                new FileOutputStream(Paths.get(directoryName, files[i + j].name).toFile()))) {
              decodeFollowingFile(out, files[i + j], files[i + j - 1]);
            }
          }
          for (int j = 0; j < 4; j++) {
            files[i + j].discard();
          }
        }
      } finally {
        in.close();
        in = null;
      }
    }

    void decodeFirstFile(OutputStream out, ImageFile file) throws Exception {
      final int w = file.width();
      final int h = file.height();
      RangeCoder decoder = RangeCoder.read(in);
      file.values = new int[h + 2][w + 2];
      int[] diff = new int[h * w];
      long startTime = System.currentTimeMillis();
      decoder.decode(in, diff, h * w);
      System.err.println("decode " + (System.currentTimeMillis() - startTime));
      for (int i = 1; i <= h; i++) {
        for (int j = 1; j <= w; j++) {
          file.values[i][j] = file.values[i][j - 1] + (file.values[i - 1][j] - file.values[i - 1][j - 1]) + diff[(i - 1) * w + j - 1] - SHIFT;
          write16(out, file.values[i][j]);
        }
      }
      System.err.println("output " + (System.currentTimeMillis() - startTime));
    }

    void decodeFollowingFile(OutputStream out, ImageFile file, ImageFile prevFile) throws Exception {
      long startTime = System.currentTimeMillis();
      final int w = file.width();
      final int h = file.height();
      int[][] move = new int[h / BLOCK][w / BLOCK];
      for (int i = 0; i < h / BLOCK; i++) {
        for (int j = 0; j < w / BLOCK; j++) {
          move[i][j] = read16(in);
        }
      }
      RangeCoder decoder = RangeCoder.read(in);
      file.values = new int[h + 2][w + 2];
      int[] diff = new int[h * w];
      decoder.decode(in, diff, h * w);
      System.err.println("decode " + (System.currentTimeMillis() - startTime));

      for (int bi = 0; bi < h / BLOCK; bi++) {
        for (int bj = 0; bj < w / BLOCK; bj++) {
          int dy = (move[bi][bj] >> 8) - 127;
          int dx = (move[bi][bj] & 0xFF) - 127;
          for (int i = 0; i < BLOCK; i++) {
            int cy = bi * BLOCK + i + 1;
            for (int j = 0; j < BLOCK; j++) {
              int cx = bj * BLOCK + j + 1;
              file.values[cy][cx] = file.values[cy][cx - 1]
                  + (prevFile.values[cy + dy][cx + dx] - prevFile.values[cy + dy][cx + dx - 1])
                  + diff[(cy - 1) * w + cx - 1] - SHIFT;
            }
          }
        }
      }

      for (int i = 1; i <= h; i++) {
        for (int j = 1; j <= w; j++) {
          write16(out, file.values[i][j]);
        }
      }
      System.err.println("output " + (System.currentTimeMillis() - startTime));
    }

    ImageFile[] readHeader() throws Exception {
      int N = in.read();
      ImageFile[] ret = new ImageFile[N];
      for (int i = 0; i < N; ++i) {
        ret[i] = readFileInfo();
      }
      return ret;
    }

    ImageFile readFileInfo() throws Exception {
      int b = in.read();
      String name = "";
      while (b != 0) {
        name += (char) b;
        b = in.read();
      }
      int size = in.read();
      size |= in.read() << 8;
      size |= in.read() << 16;
      size |= in.read() << 24;
      return new ImageFile(name, size);
    }

  }

  public static void main(String[] args) throws Exception {
    try (Scanner sc = new Scanner(System.in)) {
      String type = sc.next();
      encodingFileName = sc.next();
      directoryName = sc.next();
      if (type.equals("encode")) {
        int N = sc.nextInt();
        ImageFile[] files = new ImageFile[N];
        for (int i = 0; i < N; ++i) {
          String name = sc.next();
          int size = sc.nextInt();
          files[i] = new ImageFile(name, size);
        }
        Arrays.sort(files, (ImageFile f1, ImageFile f2) -> f1.name.compareTo(f2.name));
        Encoder encoder = new Encoder();
        encoder.encode(files);
      } else {
        Decoder decoder = new Decoder();
        decoder.decode();
      }
    }
  }

  private static int read16(InputStream in) {
    try {
      int lo = in.read();
      return (in.read() << 8) | lo;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static void write16(OutputStream out, int v) {
    try {
      out.write(v);
      out.write(v >> 8);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  static class ImageFile {
    String name;
    int size;
    int[][] values;

    ImageFile(String name, int size) {
      this.name = name;
      this.size = size;
    }

    void read() {
      long startTime = System.currentTimeMillis();
      this.values = new int[this.height() + 2][this.width() + 2];
      try (InputStream in = new BufferedInputStream(
          new FileInputStream(Paths.get(directoryName, this.name).toFile()))) {
        int w = this.width();
        int h = this.height();
        for (int i = 0; i < h; i++) {
          for (int j = 0; j < w; j++) {
            this.values[i + 1][j + 1] = read16(in);
          }
        }
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
      System.err.println("file read " + (System.currentTimeMillis() - startTime));
    }

    void discard() {
      this.values = null;
    }

    int height() {
      return this.size / 2 / this.width();
    }

    int width() {
      if (size == 2 * 5000 * 3000) {
        return 5000;
      } else if (size == 2 * 2500 * 1500) {
        return 2500;
      } else {
        return 10000;
      }
    }
  }
}
