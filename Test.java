import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Test {

  public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
      sc.next();
      Main.encodingFileName = sc.next();
      Main.directoryName = sc.next();
      int N = sc.nextInt();
      Main.ImageFile[] files = new Main.ImageFile[N];
      for (int i = 0; i < N; ++i) {
        String name = sc.next();
        int size = sc.nextInt();
        files[i] = new Main.ImageFile(name, size);
        files[i].read();
        test(files[i], String.format("diffs/%2d_%d.txt", i / 4, i % 4));
        files[i].discard();
      }
    }
  }

  static void test(Main.ImageFile file, String outFileName) {
    try (PrintWriter writer = new PrintWriter(new File(outFileName))) {
      for (int i = 0; i < file.height(); i++) {
        for (int j = 0; j < file.width(); j++) {
          int diff = file.values[i + 1][j + 1] - file.values[i + 1][j] - (file.values[i][j + 1] - file.values[i][j]);
          writer.print(diff < 0 ? '-' : ' ');
          writer.printf("%3x ", Math.abs(diff));
        }
        writer.println();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
//    BufferedImage image = new BufferedImage(file.width(), file.height(), BufferedImage.TYPE_BYTE_GRAY);
//    int max = 0;
//    for (int i = 0; i < image.getHeight(); i++) {
//      for (int j = 0; j < image.getWidth(); j++) {
//        max = Math.max(max, file.values[i + 1][j + 1]);
//      }
//    }
//    System.err.printf("max:%x\n", max);
//    int shift = Math.max(0, 24 - Integer.numberOfLeadingZeros(max));
//    for (int i = 0; i < image.getHeight(); i++) {
//      for (int j = 0; j < image.getWidth(); j++) {
//        int v = Math.min(0xFF, file.values[i + 1][j + 1] >> shift);
//        image.setRGB(j, i, (v << 16) | (v << 8) | v);
//      }
//    }
//    try {
//      ImageIO.write(image, "png", new File(outFileName));
//    } catch (IOException e) {
//      throw new RuntimeException(e);
//    }
  }
}
