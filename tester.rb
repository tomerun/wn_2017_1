require 'fileutils'
require 'open3'

cwd = Dir.getwd()
in_dir = cwd + '/input/bin_4obs'
out_dir = cwd + '/output'
encode_dir = cwd + '/encode'
encode_path = encode_dir + '/compressed.dat'

# clean up
FileUtils.remove(Dir.glob(out_dir + '/*'))
FileUtils.remove(Dir.glob(encode_dir + '/*'))

# encode
in_files = Dir.glob(in_dir + '/*.bin')
input_data = ['encode', encode_path, in_dir, in_files.size.to_s]
in_files.each do |f|
	input_data << (File.basename(f) + ' ' + File.size(f).to_s)
end
Open3.popen3('java -Xms2G -Xmx4G Main') do |stdin, stdout, stderr, wait_thr|
	stdin.puts(input_data.join("\n"))
	stdin.close()
end

# decode
input_data = ['decode', encode_path, out_dir]
Open3.popen3('java -Xmx4G Main') do |stdin, stdout, stderr, wait_thr|
	stdin.puts(input_data.join("\n"))
	stdin.close()
end

# result check
in_files.each do |in_file|
	name = File.basename(in_file)
	out_file = out_dir + '/' + name
	if !File.exist?(out_file)
		puts "does not exist:#{out_file}"
	elsif !FileUtils.cmp(in_file, out_file)
		puts "different:#{in_file} #{out_file}"
	end
end
compressed_size = File.size(encode_path);
puts "compressed size:#{compressed_size}"
puts "score to bzip:#{ 532905020.0 / compressed_size}"
puts "score to orig:#{1200000000.0 / compressed_size}"
